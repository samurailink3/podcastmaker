package podcastmaker

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestFeedItem_ParseUploadDate(t *testing.T) {
	goodTestCase := FeedItem{UploadDate: "20060102"}
	got, err := goodTestCase.ParseUploadDate()
	assert.NoError(t, err)
	expected, err := time.Parse(time.RFC3339, "2006-01-02T00:00:00Z")
	assert.NoError(t, err)
	assert.Equal(t, expected, got)

	errorTestCase := FeedItem{UploadDate: "00000000"}
	_, err = errorTestCase.ParseUploadDate()
	assert.Error(t, err)
}

func TestFeedItem_Thumbnail(t *testing.T) {
	tests := []struct {
		given    FeedItem
		expected string
	}{
		{
			given:    FeedItem{ID: "abcd"},
			expected: "https://i.ytimg.com/vi/abcd/maxresdefault.jpg",
		},
		{
			given:    FeedItem{ID: "1234"},
			expected: "https://i.ytimg.com/vi/1234/maxresdefault.jpg",
		},
		{
			given:    FeedItem{ID: ""},
			expected: "https://i.ytimg.com/vi//maxresdefault.jpg",
		},
	}

	for _, testCase := range tests {
		assert.Equal(t, testCase.expected, testCase.given.Thumbnail())
	}
}
