package podcastmaker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetUUIDFromURL(t *testing.T) {
	videoURLUUID := GetUUIDFromURL("https://www.example.com")
	assert.Equal(t, "a0787afd-c170-5773-4d60-02739168de9f", videoURLUUID)
}
