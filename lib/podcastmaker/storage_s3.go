package podcastmaker

import (
	"bytes"
	"log"
	"math"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

const (
	partSize = 104857600 // 100 MiB Part Size
)

// S3 is an S3-compatible StorageEngine with optional CDN support.
type S3 struct {
	basePath   string // What each path should be prefixed with.
	cdnURL     string // The CDN URL files should be served from. Optional.
	endpoint   string // The S3 endpoint we should connect to.
	bucketName string // Your bucket name.
	s3SVC      *s3.S3
	tempDir    string // Where videos are locally stored before being shipped to the S3 bucket.
}

// NewS3 returns a fully-configured S3 StorageEngine.
func NewS3(basePath string, region string, endpoint string, bucketName string, useEnvironmentCreds bool, accessKey string, secretKey string, sessionToken string, cdnURL string, tempDirPath string) *S3 {
	var sess *session.Session
	var err error
	if useEnvironmentCreds {
		sess, err = session.NewSession()
		if err != nil {
			panic(err)
		}
	} else {
		sess = session.Must(session.NewSession(&aws.Config{
			Endpoint:    &endpoint,
			Region:      &region,
			Credentials: credentials.NewStaticCredentials(accessKey, secretKey, sessionToken),
		}))
	}
	svc := s3.New(sess)
	return &S3{
		basePath:   basePath,
		cdnURL:     cdnURL,
		endpoint:   endpoint,
		bucketName: bucketName,
		s3SVC:      svc,
		tempDir:    tempDirPath,
	}
}

// BasePath returns the configured path prefix.
func (s *S3) BasePath() string {
	return s.basePath
}

// CreateDirectory creates a directory in the configured bucket.
func (s *S3) CreateDirectory(path string) (err error) {
	// S3 directories must end in a "/"
	// ref: https://docs.aws.amazon.com/AmazonS3/latest/user-guide/using-folders.html
	fullPath := filepath.Join(s.BasePath(), path) + "/"
	_, err = s.s3SVC.PutObject(&s3.PutObjectInput{
		ACL:    aws.String("private"),
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(fullPath),
	})
	if err != nil {
		return err
	}
	return nil
}

// FileServerPath returns a fully-qualified path to the bucket on the configured
// S3 endpoint.
func (s *S3) FileServerPath() string {
	if len(s.cdnURL) != 0 {
		return s.cdnURL + "/"
	}
	return "https://" + s.bucketName + "." + s.endpoint + "/"
}

// PutFile uploads data to a specific S3 key in the configured bucket. This
// function utilizes S3 Multipart Upload. Each file is uploaded in 100 MiB
// chunks (controlled by the `partSize` constant) and finalized once the final
// chunk is uploaded. This is necessary for any video file that exceeds 5GB.
func (s *S3) PutFile(filename string, fileData []byte) (err error) {
	fullPath := filepath.Join(s.BasePath(), filename)

	// Tell S3 we're going to upload a single file in multiple parts.
	multipartUploadResponse, err := s.s3SVC.CreateMultipartUpload(&s3.CreateMultipartUploadInput{
		ACL:         aws.String("public-read"),
		Bucket:      aws.String(s.bucketName),
		Key:         aws.String(fullPath),
		ContentType: aws.String("video/mp4"),
	})
	if err != nil {
		return err
	}

	// Create a slice to contain the completed parts so we can give this to S3
	// to finalize the upload later.
	uploadedParts := []*s3.CompletedPart{}
	// Do some math to figure out how many chunks we need to upload.
	numberOfParts := int(math.Ceil(float64(len(fileData)) / float64(partSize)))

	for i := 0; i < numberOfParts; i++ {
		// Each upload has a specific part number
		partNumber := i + 1
		// Since we have `fileData` in a byte-slice, we can specify data offsets
		// to upload consistent part sizes.
		dataIndexStart := i * partSize
		dataIndexEnd := i*partSize + partSize
		// The only exception to this consistency is if we're uploading the last
		// chunk. If it isn't exactly 100 MiB, we set the final ending index to
		// the last byte in the `fileData` slice.
		if len(fileData) < dataIndexEnd {
			dataIndexEnd = len(fileData) + 1
		}

		// These uploads can take a while, so let's give some feedback.
		log.Printf("Uploading: %s Part %d/%d data[%d:%d]\n", fullPath, partNumber, numberOfParts, dataIndexStart, dataIndexEnd)
		// Actually upload the part.
		uploadPartResponse, err := s.s3SVC.UploadPart(&s3.UploadPartInput{
			Body:          bytes.NewReader(fileData[dataIndexStart:dataIndexEnd]),
			Bucket:        aws.String(s.bucketName),
			Key:           aws.String(fullPath),
			PartNumber:    aws.Int64(int64(partNumber)),
			UploadId:      multipartUploadResponse.UploadId,
			ContentLength: aws.Int64(int64(len(fileData[dataIndexStart:dataIndexEnd]))),
		})
		if err != nil {
			return err
		}

		// After the upload succeeds, add its metadata to our uploadedParts
		// slice.
		uploadedParts = append(uploadedParts, &s3.CompletedPart{
			ETag:       uploadPartResponse.ETag,
			PartNumber: aws.Int64(int64(partNumber)),
		})
	}

	// Once all the parts have been uploaded, let S3 know we're done with the
	// multipart upload by giving it the UploadID and our list of completed
	// Parts.
	_, err = s.s3SVC.CompleteMultipartUpload(&s3.CompleteMultipartUploadInput{
		Bucket:   aws.String(s.bucketName),
		Key:      aws.String(fullPath),
		UploadId: multipartUploadResponse.UploadId,
		MultipartUpload: &s3.CompletedMultipartUpload{
			Parts: uploadedParts,
		},
	})
	if err != nil {
		return err
	}

	return nil
}

// DoesFileExist will return whether or not a key exists in S3.
func (s *S3) DoesFileExist(absolutePath string) bool {
	_, err := s.s3SVC.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(absolutePath),
	})
	return err == nil
}

// FileSize will return the file size of an object stored in S3.
func (s *S3) FileSize(filename string) (fileSizeInBytes int64, err error) {
	fullPath := filepath.Join(s.BasePath(), filename)
	output, err := s.s3SVC.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(fullPath),
	})
	if err != nil {
		return 0, err
	}
	return *output.ContentLength, nil
}

// TempDir returns where temporary video downloads should be stored locally
// before upload.
func (s *S3) TempDir() string {
	return s.tempDir
}
