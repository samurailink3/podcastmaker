package podcastmaker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInspect(t *testing.T) {
	Inspect("TestString", "TestString")
}

func Test_cleanStringSlice(t *testing.T) {
	tests := []struct {
		given    []string
		expected []string
	}{
		{
			given:    []string{"", "one", "two"},
			expected: []string{"one", "two"},
		},
		{
			given:    []string{"one", "", "two"},
			expected: []string{"one", "two"},
		},
		{
			given:    []string{"one", "two", ""},
			expected: []string{"one", "two"},
		},
		{
			given:    []string{"", "one", "", "two", ""},
			expected: []string{"one", "two"},
		},
		{
			given:    []string{"", "", ""},
			expected: []string{},
		},
		{
			given:    []string{"one", "two"},
			expected: []string{"one", "two"},
		},
	}
	for _, testCase := range tests {
		assert.Equal(t, testCase.expected, cleanStringSlice(testCase.given))
	}
}
