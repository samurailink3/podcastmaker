package podcastmaker

import (
	"errors"
)

var (
	// NullStorageError is returned whenever a bad config value for
	// StorageEngineType is used.
	NullStorageError = errors.New("bad config value for StorageEngineType")
)

// StorageEngine controls how PodcastMaker stores, uploads, and serves video
// files.
type StorageEngine interface {
	BasePath() string                                            // What each download path should be prefixed with
	FileServerPath() string                                      // Returns a fully-qualified path to the video-serving host
	PutFile(filename string, fileData []byte) (err error)        // Uploads a video file
	CreateDirectory(path string) (err error)                     // Creates a directory
	DoesFileExist(absolutePath string) bool                      // Identifies if a particular filename exists
	FileSize(filename string) (fileSizeInBytes int64, err error) // Returns the size of a file
	TempDir() string                                             // Where videos should be temporarily stored before upload
}

// NullStorage is used to provide error returns when reading bad
// StorageEngineType values in Config structs.
type NullStorage struct{}

func (n NullStorage) BasePath() string       { return "" }
func (n NullStorage) FileServerPath() string { return "" }
func (n NullStorage) PutFile(filename string, fileData []byte) (err error) {
	return NullStorageError
}
func (n NullStorage) CreateDirectory(path string) (err error) { return NullStorageError }
func (n NullStorage) DoesFileExist(absolutePath string) bool  { return true }
func (n NullStorage) FileSize(filename string) (fileSizeInBytes int64, err error) {
	return 0, NullStorageError
}
func (n NullStorage) TempDir() string { return "" }
