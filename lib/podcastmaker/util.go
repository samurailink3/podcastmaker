package podcastmaker

import "log"

// Inspect is a handy utility to print debug various objects.
func Inspect(name string, object interface{}) {
	log.Printf("%s (%T): %#v\n", name, object, object)
}

// cleanStringSlice will clean string slices and remove empty elements.
func cleanStringSlice(stringSlice []string) (cleanedStringSlice []string) {
	cleanedStringSlice = []string{}
	for _, str := range stringSlice {
		if len(str) > 0 {
			cleanedStringSlice = append(cleanedStringSlice, str)
		}
	}
	return cleanedStringSlice
}
