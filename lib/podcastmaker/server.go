package podcastmaker

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// HomePage controls the content shown on the index page of PodcastMaker if the
// `ShowFrontPage` option is enabled in the Config.
const HomePage = `Welcome to Podcast Maker!

To convert a YouTube channel to a podcast feed, try URLs like this:
/youtube/user/USER_NAME
/youtube/channel/CHANNEL_ID
/youtube/c/CHANNEL_ID
/youtube/playlist?list=PLAYLIST_ID

If the request is taking a while, just wait, we're downloading the channel videos in the background.

If you need a new URL for the same feed, you can add random text to the end of your URL, like this:
/youtube/user/USER_NAME/some-random-text
This URL will give you the same content as: /youtube/user/USER_NAME
This is useful for cache-busting or if your feeds have broken in your podcast app of choice. Just
change the random text and you have a totally 'new' feed, without needing to re-download videos.
`

// StartServer will start podcastmaker's webserver and listen for incoming
// requests. As URLs are requested, PodcastMaker will automatically obtain a
// list of the most recent uploaded videos, download them, and push them to the
// configured StorageEngine.
func StartServer(config Config, storageEngine StorageEngine) {
	serverAddress := config.ServerHost + ":" + strconv.Itoa(config.ServerPort)

	mux := http.NewServeMux()

	mux.HandleFunc("/youtube/", func(w http.ResponseWriter, req *http.Request) {
		LogHTTPRequest(req)

		urlArgs := cleanStringSlice(strings.Split(req.URL.String(), "/"))

		// We need special logic to break apart playlist arguments
		if len(urlArgs) == 2 && len(urlArgs[1]) >= 8 && urlArgs[1][:8] == "playlist" {
			playlistArgs := strings.Split(urlArgs[1], "?")
			if len(playlistArgs) != 2 {
				http.NotFound(w, req)
				return
			}
			urlArgs[1] = "playlist"
			urlArgs = append(urlArgs, "?"+playlistArgs[1])
		}

		if len(urlArgs) < 3 {
			http.NotFound(w, req)
			return
		}

		category := urlArgs[1]
		id := urlArgs[2]
		if id == "favicon.ico" {
			http.NotFound(w, req)
			return
		}

		var channel string
		switch category {
		case "playlist":
			channel = "https://www.youtube.com/" + category + id
		default:
			channel = "https://www.youtube.com/" + category + "/" + id + "/videos"
		}
		feed, err := DownloadVideosForChannel(&config, channel, storageEngine)
		if err != nil {
			log.Println(err)
			fmt.Fprintln(w, "podcastmaker encountered an error while downloading videos for this channel: "+channel)
			return
		}
		genFeed, err := GenerateFeed(feed, storageEngine)
		if err != nil {
			log.Println(err)
			fmt.Fprintln(w, "podcastmaker encountered an error while generating a feed for this channel: "+channel)
			return

		}
		rss, err := genFeed.ToRss()
		if err != nil {
			log.Println(err)
			fmt.Fprintln(w, "podcastmaker encountered an error while building an RSS feed for this channel: "+channel)
			return
		}
		fmt.Fprint(w, rss)
	})

	if config.StorageEngineType == "LocalStorage" {
		fileServerURL, err := url.Parse(storageEngine.FileServerPath())
		if err != nil {
			log.Fatal(err)
		}
		mux.Handle(fileServerURL.EscapedPath(), fileSystemLogger(http.StripPrefix(fileServerURL.EscapedPath(), http.FileServer(http.Dir(storageEngine.BasePath())))))
	}

	if config.ShowFrontPage {
		mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
			LogHTTPRequest(req)

			if req.URL.Path != "/" {
				http.NotFound(w, req)
				return
			}
			fmt.Fprint(w, HomePage)
		})
	}

	log.Println("Listening on", "http://"+serverAddress)
	log.Fatal(http.ListenAndServe(serverAddress, mux))
}

// fileSystemLogger logs requests made against the LocalStorage StorageEngine.
func fileSystemLogger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		LogHTTPRequest(req)
		handler.ServeHTTP(w, req)
	})
}

// LogHTTPRequest is a small helper function designed to make logging HTTP
// requests consistent and easy.
func LogHTTPRequest(req *http.Request) {
	log.Println(req.RemoteAddr, req.Method, req.Host+req.URL.Path)
}
