module gitlab.com/samurailink3/podcastmaker/lib/podcastmaker

go 1.16

require (
	github.com/aws/aws-sdk-go v1.44.47
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/feeds v1.1.1
	github.com/kr/pretty v0.1.0 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/stretchr/testify v1.5.1
	gopkg.in/yaml.v3 v3.0.1
)
