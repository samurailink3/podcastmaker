package podcastmaker

const (
	YoutubeRSSRegexp    = `https://www.youtube.com/feeds/videos.xml\?[A-Za-z0-9_=-]+`
	YoutubeAvatarRegexp = `"avatar":(.*?),"banner":`
	FileExtension       = ".mp4"
)
