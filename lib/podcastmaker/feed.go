package podcastmaker

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/feeds"
)

// GetAvatarForShow will return a URL to the channel's image.
func GetAvatarForShow(youtubeURL string) (avatarURL string) {
	resp, err := http.Get(youtubeURL)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	ytAvatarRegexp := regexp.MustCompile(YoutubeAvatarRegexp)
	avatarMatches := ytAvatarRegexp.FindStringSubmatch(string(body))

	return avatarMatches[0]
}

// GenerateFeed will create an RSS feed from a slice of `FeedItem`s and a
// `StorageEngine`.
func GenerateFeed(feed []FeedItem, storageEngine StorageEngine) (generatedFeed feeds.Feed, err error) {
	// Safety check
	if len(feed) == 0 {
		return feeds.Feed{}, nil
	}

	// Collect base feed information
	now := time.Now()
	avatarURL := feed[0].AvatarURL()
	channelTitle := feed[0].Channel
	channelURL := feed[0].ChannelURL
	channelDescription := feed[0].Playlist
	uploader := feed[0].UploaderID

	// Generate base feed
	generatedFeed = feeds.Feed{
		Title:       channelTitle,
		Link:        &feeds.Link{Href: channelURL},
		Description: channelDescription,
		Author:      &feeds.Author{Name: uploader},
		Image: &feeds.Image{
			Title: channelTitle,
			Url:   avatarURL,
			Link:  channelURL,
		},
		Created: now,
	}

	// Generate feed item metadata
	for _, item := range feed {
		filename := GetUUIDFromURL(item.URL) + FileExtension
		fileLength, err := storageEngine.FileSize(filename)
		if err != nil {
			return feeds.Feed{}, err
		}
		itemDescription := strings.ReplaceAll(item.Description, "\n", "<br/>")
		var itemCreated time.Time
		itemCreated, err = item.ParseUploadDate()
		if err != nil {
			log.Println("Error parsing item date, using blank date instead:", err)
		}

		generatedFeed.Items = append(generatedFeed.Items, &feeds.Item{
			Title:       item.Title,
			Link:        &feeds.Link{Href: item.URL},
			Description: item.Description,
			Created:     itemCreated,
			Content:     "Original Link: <a href=\"" + item.URL + "\">" + item.URL + "</a><br/><br/>" + itemDescription,
			Id:          item.URL,
			Enclosure: &feeds.Enclosure{
				Url:    storageEngine.FileServerPath() + filename,
				Length: strconv.FormatInt(fileLength, 10),
				Type:   "video/mp4",
			},
		})
	}

	return generatedFeed, nil
}

// FeedItem is used to unmarshal JSON returned from `yt-dlp -j <URL>` into a
// usable format.
type FeedItem struct {
	ID          string `json:"id"`
	Channel     string `json:"channel"`
	ChannelURL  string `json:"channel_url"`
	Description string `json:"description"`
	Playlist    string `json:"playlist"`
	Title       string `json:"title"`
	UploadDate  string `json:"upload_date"`
	UploaderID  string `json:"uploader_id"`
	URL         string `json:"webpage_url"`
}

// ParseUploadDate will return the upload date as a `time.Time` object.
func (fi *FeedItem) ParseUploadDate() (t time.Time, err error) {
	t, err = time.Parse("20060102", fi.UploadDate)
	if err != nil {
		return time.Time{}, err
	}
	return t, nil
}

// Thumbnail will return the link for the largest resolution video thumbnail
// available.
func (fi *FeedItem) Thumbnail() string {
	return "https://i.ytimg.com/vi/" + fi.ID + "/maxresdefault.jpg"
}

// AvatarURL returns a URL to the channel's image.
func (fi *FeedItem) AvatarURL() string {
	type thumbnail struct {
		URL    string
		Width  int
		Height int
	}
	type thumbnails struct {
		Thumbnails []thumbnail
	}
	var t thumbnails
	resp, err := http.Get(fi.ChannelURL)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	ytAvatarRegexp := regexp.MustCompile(YoutubeAvatarRegexp)
	thumbnailJSON := ytAvatarRegexp.FindStringSubmatch(string(body))
	if len(thumbnailJSON) == 0 {
		return ""
	}
	err = json.Unmarshal([]byte(thumbnailJSON[1]), &t)
	if err != nil {
		log.Println("Could not unmarshal thumbnails for channel, using blank:", err)
	}

	var largestThumbnail thumbnail
	for _, item := range t.Thumbnails {
		if item.Height > largestThumbnail.Height {
			largestThumbnail = item
		}
	}

	return largestThumbnail.URL
}
