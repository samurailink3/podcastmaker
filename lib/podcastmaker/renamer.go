package podcastmaker

import uuid "github.com/nu7hatch/gouuid"

// GetUUIDFromURL will convert a URL into a predictable filename. This function
// relies on UUID v5 namespaces to deterministically convert a given URL into a
// consistent UUID.
// Ref: https://en.wikipedia.org/wiki/Universally_unique_identifier#Versions_3_and_5_(namespace_name-based)
func GetUUIDFromURL(url string) (filename string) {
	videoURLUUID, err := uuid.NewV5(uuid.NamespaceURL, []byte(url))
	if err != nil {
		panic(err)
	}
	return videoURLUUID.String()
}
