package podcastmaker

import (
	"io/ioutil"
	"os"
	"path/filepath"
)

// LocalStorage allows the use of the local filesystem to store PodcastMaker
// downloads.
type LocalStorage struct {
	basePath       string // What each path should be prefixed with.
	fileServerPath string // The full hostname we should serve file with. This is used by GenerateFeed() for enclosure paths.
	tempDirPath    string // Where yt-dlp will initially download files and fragments, moved into basePath when it completes.
}

// NewLocalStorage configures and returns a LocalStorage StorageEngine.
func NewLocalStorage(relativeBasePath string, fileServerPath string, tempDirPath string) *LocalStorage {
	basePath, err := filepath.Abs(relativeBasePath)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(basePath, os.ModePerm)
	if err != nil {
		panic(err)
	}
	tempDirPath, err = filepath.Abs(tempDirPath)
	if err != nil {
		panic(err)
	}

	return &LocalStorage{basePath: basePath, fileServerPath: fileServerPath, tempDirPath: tempDirPath}
}

// BasePath returns the configured path prefix.
func (l *LocalStorage) BasePath() string {
	return l.basePath
}

// FileServerPath returns the configured hostname.
func (l *LocalStorage) FileServerPath() string {
	return l.fileServerPath
}

// PutFile writes data to a specified path on a filesystem.
func (l *LocalStorage) PutFile(filename string, fileData []byte) (err error) {
	fullpath := filepath.Join(l.BasePath(), filename)
	err = ioutil.WriteFile(fullpath, fileData, 0400)
	if err != nil {
		return err
	}
	return nil
}

// CreateDirectory creates a directory on the configured filesystem.
func (l *LocalStorage) CreateDirectory(relativePath string) (err error) {
	err = os.MkdirAll(filepath.Join(l.BasePath(), relativePath), 0700)
	if err != nil {
		return err
	}
	return nil
}

// DoesFileExist will return if a file exists or not.
func (l *LocalStorage) DoesFileExist(absolutePath string) bool {
	if _, err := os.Stat(absolutePath); os.IsNotExist(err) {
		return false
	}
	return true
}

// FileSize returns the size of a file in bytes.
func (l *LocalStorage) FileSize(filename string) (fileSizeInBytes int64, err error) {
	fullpath := filepath.Join(l.BasePath(), filename)
	fileInfo, err := os.Stat(fullpath)
	if err != nil {
		return 0, err
	}
	return fileInfo.Size(), nil
}

// TempDir returns where temporary video downloads should be stored.
func (l *LocalStorage) TempDir() string {
	return l.tempDirPath
}
