package podcastmaker

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// DownloadVideosForChannel will download all videos for a particular channel.
// This will only download the 5 most recent videos.
func DownloadVideosForChannel(config *Config, channelURL string, storageEngine StorageEngine) (feed []FeedItem, err error) {
	log.Println("Getting videos for:", channelURL)
	ytdl := exec.Command("yt-dlp", "-j", "--max-downloads=5", channelURL)
	log.Println("Running:", ytdl.String())
	output, err := ytdl.Output()
	if err != nil {
		log.Println("Non-zero exit code returned from yt-dlp. This may not be a problem:", err)
	}
	outputStrings := cleanStringSlice(strings.Split(string(output), "\n"))
	for _, feedEntryJSON := range outputStrings {
		var feedEntry FeedItem
		err := json.Unmarshal([]byte(feedEntryJSON), &feedEntry)
		if err != nil {
			return []FeedItem{}, err
		}

		fullPath := filepath.Join(storageEngine.BasePath(), GetUUIDFromURL(feedEntry.URL)+FileExtension)
		if storageEngine.DoesFileExist(fullPath) {
			log.Println("Looks like I already have:", feedEntry.Title, "("+fullPath+")")
		} else {
			log.Println("I need to download a file:", feedEntry.Title, "("+fullPath+")")
			err := DownloadVideo(feedEntry.URL, storageEngine, true)
			if err != nil {
				return []FeedItem{}, err
			}
			log.Println("Done downloading:", feedEntry.Title, "("+fullPath+")")
		}

		feed = append(feed, feedEntry)
	}

	return feed, nil
}

// DownloadVideo invokes `yt-dlp` to download a video after a randomly-selected
// delay (in order to avoid throttling), uploads it to the configured
// StorageEngine, then deletes the temporary local copy.
func DownloadVideo(url string, storageEngine StorageEngine, useRandomSleep bool) error {
	filename := GetUUIDFromURL(url) + FileExtension
	filepath := filepath.Join(storageEngine.TempDir(), filename)
	var ytdl *exec.Cmd
	if useRandomSleep {
		ytdl = exec.Command(
			"yt-dlp",
			"--newline",
			"--sleep-interval", "30",
			"--max-sleep-interval", "60",
			"-f", "bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4",
			"-o", filepath,
			url)
	} else {
		ytdl = exec.Command(
			"yt-dlp",
			"--newline",
			"-f", "bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4",
			"-o", filepath,
			url)
	}
	log.Println("Running:", ytdl.String())
	output, err := ytdl.CombinedOutput()
	log.Println(string(output))
	if err != nil {
		return (err)
	}

	fileData, err := ioutil.ReadFile(filepath)
	if err != nil {
		return (err)
	}

	err = storageEngine.PutFile(filename, fileData)
	if err != nil {
		return (err)
	}

	err = os.Remove(filepath)
	if err != nil {
		return (err)
	}

	return nil
}
