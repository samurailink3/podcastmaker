package podcastmaker

import (
	"flag"
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v3"
)

const (
	configFileUsage                 = "the location of your config file [ignores all other flags!]"
	showFrontPageUsage              = "if enabled, users will be shown instructions on the front page. Otherwise, they will receive a 404 (useful for personal podcastmaker instances)"
	serverHostUsage                 = "the host domain or IP you would like to listen on"
	serverPortUsage                 = "the port you would like to listen on"
	storageEngineTypeUsage          = "the type of StorageEngine are you using"
	localStorageBasePathUsage       = "the path where you want LocalStorage to store files"
	localStorageFileServerPathUsage = "the path that you want files to be served from"
	tempDirPathUsage                = "the path that you want yt-dlp files and fragments to be downloaded to initially"
	s3BucketNameUsage               = "the name of your bucket"
	s3RegionUsage                   = "the region your S3 bucket is in"
	s3EndpointUsage                 = "the S3 endpoint you're using"
	s3UseEnvironmentCredsUsage      = "use AWS credentials from environment variables"
	s3AccessKeyUsage                = "the AWS Acess Key"
	s3SecretKeyUsage                = "the AWS Secret Key"
	s3SessionTokenUsage             = "the AWS Session Token for short-lived credentials"
	s3FileDirectoryUsage            = "the subdirectory where you want to store your videos"
	cdnURLUsage                     = "the optional CDN URL for your bucket (if enabled)"
)

// Config contains every option that powers podcastmaker.
type Config struct {
	// ShowFrontPage controls whether or not you want to display the instructions
	// on the front page. If not, the user will receive a 404 error when
	// nativating to the site root.
	// Default: true
	ShowFrontPage bool `yaml:"ShowFrontPage"`
	// ServerHost determines the host domain or IP you would like to listen on.
	// Default: 0.0.0.0
	// Example: podcastmaker.example.com
	ServerHost string `yaml:"ServerHost"`
	// ServerPort controls the port you would like to listen on.
	// Default: 3000
	ServerPort int `yaml:"ServerPort"`
	// StorageEngineType determines the type of storage engine are you using.
	// Valid Storage Engines:
	//   * LocalStorage
	//   * S3
	// Default: LocalStorage
	StorageEngineType string `yaml:"StorageEngineType"`
	// LocalStorageBasePath controls the path where LocalStorage should store
	// files.
	// Default: ./downloads
	// Example: /opt/podcastmaker/dowloads
	LocalStorageBasePath string `yaml:"LocalStorageBasePath,omitempty,flow"`
	// LocalStorageFileServerPath controls the path that you want files to be
	// served from.
	// Default: http://127.0.0.1:3000/files/
	// Example: http://example.com/videos/
	LocalStorageFileServerPath string `yaml:"LocalStorageFileServerPath,omitempty"`
	// TempDirPath controls the path that you want yt-dlp files and fragments to
	// be downloaded to initially. These files will be moved to their final
	// location when the current video has finished downloading.
	// Default: ./tmp
	// Example: /opt/podcastmaker/tmp
	TempDirPath string `yaml:"TempDirPath,omitempty"`
	// S3BucketName is the name of your bucket.
	// Example: freds-podcast-maker
	S3BucketName string `yaml:"S3BucketName,omitempty"`
	// S3Region is where your bucket was created.
	// Example: us-east-1
	S3Region string `yaml:"S3Region,omitempty"`
	// S3Endpoint is the S3 endpoint you're using.
	// Example: s3.us-east-1.amazonaws.com
	S3Endpoint string `yaml:"S3Endpoint,omitempty"`
	// S3UseEnvironmentCreds allows you to toggle using S3 credentials from
	// environment variables or default credential storage locations. This option
	// disables S3AccessKey, S3SecretKey, and S3SessionToken variables. This can
	// be helpful if you're running PodcastMaker from an AWS Lambda function or
	// using assumed roles.
	// Default: false
	S3UseEnvironmentCreds bool `yaml:"S3UseEnvironmentCreds,omitempty"`
	// S3AccessKey is your API access key.
	// Example: EXAMPLEACCESSKEY
	S3AccessKey string `yaml:"S3AccessKey,omitempty"`
	// S3SecretKey is your API secret key.
	// Example: ExampleSecretKeyThatIsLongerAndProbablyHasNumbersSomewhere1234
	S3SecretKey string `yaml:"S3SecretKey,omitempty"`
	// S3SessionToken is your temporary session key. This is optional and used
	// with temporarily-assumed roles.
	// Example: ThisIsReallyLongAndChangesOften
	S3SessionToken string `yaml:"S3SessionToken,omitempty"`
	// S3FileDirectory is the subdirectory where you want to store
	// videos. Make sure this path ends in a '/'.
	// Example: /episode-files/
	S3FileDirectory string `yaml:"S3FileDirectory,omitempty"`
	// CDNURL is the optional CDN URL for your bucket (if enabled). If this option
	// is used, videos will be served from this URL instead of the default
	// endpoint directory.
	// Example: podcastmakerfiles.example.com
	CDNURL string `yaml:"CDNURL,omitempty"`
}

// Init will set up PodcastMaker flags, read a config file (if the path is
// provided), and return a new Config object ready to run BuildStorageEngine and
// get used with StartServer.
func Init() (config *Config, err error) {
	configFile := flag.String("c", "", configFileUsage)
	showFrontPage := flag.Bool("show-front-page", true, showFrontPageUsage)
	serverHost := flag.String("server-host", "0.0.0.0", serverHostUsage)
	serverPort := flag.Int("server-port", 3000, serverPortUsage)
	storageEngineType := flag.String("storage-engine-type", "LocalStorage", storageEngineTypeUsage)
	localStorageBasePath := flag.String("local-storage-base-path", "./downloads", localStorageBasePathUsage)
	localStorageFileServerPath := flag.String("local-storage-file-server-path", "http://127.0.0.1:3000/files/", localStorageFileServerPathUsage)
	tempDirPath := flag.String("temp-dir-path", "./tmp", tempDirPathUsage)
	s3BucketName := flag.String("s3-bucket-name", "", s3BucketNameUsage)
	s3Region := flag.String("s3-region", "", s3RegionUsage)
	s3Endpoint := flag.String("s3-endpoint", "", s3EndpointUsage)
	s3UseEnvironmentCreds := flag.Bool("s3-use-environment-creds", false, s3UseEnvironmentCredsUsage)
	s3AccessKey := flag.String("s3-access-key", "", s3AccessKeyUsage)
	s3SecretKey := flag.String("s3-secret-key", "", s3SecretKeyUsage)
	s3SessionToken := flag.String("s3-session-token", "", s3SessionTokenUsage)
	s3FileDirectory := flag.String("s3-file-directory", "", s3FileDirectoryUsage)
	cdnURL := flag.String("cdn-url", "", cdnURLUsage)
	flag.Parse()

	if len(*configFile) != 0 {
		config, err = ReadConfig(*configFile)
		if err != nil {
			return &Config{}, err
		}
		log.Println("Using config file at:", *configFile)
		return config, nil
	}

	config = &Config{
		ShowFrontPage:              *showFrontPage,
		ServerHost:                 *serverHost,
		ServerPort:                 *serverPort,
		StorageEngineType:          *storageEngineType,
		LocalStorageBasePath:       *localStorageBasePath,
		LocalStorageFileServerPath: *localStorageFileServerPath,
		TempDirPath:                *tempDirPath,
		S3BucketName:               *s3BucketName,
		S3Region:                   *s3Region,
		S3Endpoint:                 *s3Endpoint,
		S3UseEnvironmentCreds:      *s3UseEnvironmentCreds,
		S3AccessKey:                *s3AccessKey,
		S3SecretKey:                *s3SecretKey,
		S3SessionToken:             *s3SessionToken,
		S3FileDirectory:            *s3FileDirectory,
		CDNURL:                     *cdnURL,
	}

	log.Printf("Using flags for config: %#v\n", config)
	return config, nil
}

// ReadConfig reads a file into a Config object.
func ReadConfig(filepath string) (config *Config, err error) {
	fileData, err := ioutil.ReadFile(filepath)
	if err != nil {
		return &Config{}, err
	}
	err = yaml.Unmarshal(fileData, &config)
	if err != nil {
		return &Config{}, err
	}
	return config, nil
}

// BuildStorageEngine will configure a StorageEngine given the options in a
// Config object.
func (c *Config) BuildStorageEngine() (storageEngine StorageEngine, err error) {
	switch c.StorageEngineType {
	case "LocalStorage":
		return NewLocalStorage(c.LocalStorageBasePath, c.LocalStorageFileServerPath, c.TempDirPath), nil
	case "S3":
		return NewS3(c.S3FileDirectory, c.S3Region, c.S3Endpoint, c.S3BucketName, c.S3UseEnvironmentCreds, c.S3AccessKey, c.S3SecretKey, c.S3SessionToken, c.CDNURL, c.TempDirPath), nil
	}
	return NullStorage{}, NullStorageError
}
