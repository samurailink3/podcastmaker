package podcastmaker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadConfig(t *testing.T) {
	_, err := ReadConfig("./bad_path_that_doesnt_exist")
	assert.NotNil(t, err)
	config, err := ReadConfig("./config_example_LocalStorage.yml")
	assert.Nil(t, err)
	assert.Equal(t, true, config.ShowFrontPage)
	assert.Equal(t, "0.0.0.0", config.ServerHost)
	assert.Equal(t, 3000, config.ServerPort)
	assert.Equal(t, "./tmp", config.TempDirPath)
	assert.Equal(t, "LocalStorage", config.StorageEngineType)
	assert.Equal(t, "./downloads", config.LocalStorageBasePath)
	assert.Equal(t, "http://127.0.0.1:3000/files/", config.LocalStorageFileServerPath)

	config, err = ReadConfig("./config_example_S3.yml")
	assert.Nil(t, err)
	assert.Equal(t, true, config.ShowFrontPage)
	assert.Equal(t, "podcastmaker.example.com", config.ServerHost)
	assert.Equal(t, 443, config.ServerPort)
	assert.Equal(t, "/tmp/podcastmaker", config.TempDirPath)
	assert.Equal(t, "S3", config.StorageEngineType)
	assert.Equal(t, "podcastmaker-files-example-com", config.S3BucketName)
	assert.Equal(t, "us-east-1", config.S3Region)
	assert.Equal(t, "s3.us-east-1.amazonaws.com", config.S3Endpoint)
	assert.Equal(t, false, config.S3UseEnvironmentCreds)
	assert.Equal(t, "EXAMPLEACCESSKEY", config.S3AccessKey)
	assert.Equal(t, "ExampleSecretKeyThatIsLongerAndProbablyHasNumbersSomewhere1234", config.S3SecretKey)
	assert.Equal(t, "ThisIsReallyLongAndChangesOften", config.S3SessionToken)
	assert.Equal(t, "/episode-files/", config.S3FileDirectory)
	assert.Equal(t, "https://files.example.com", config.CDNURL)
}

func TestBuildStorageEngineLocalStorage(t *testing.T) {
	config, err := ReadConfig("./config_example_LocalStorage.yml")
	assert.Nil(t, err)
	ls, err := config.BuildStorageEngine()
	assert.Nil(t, err)
	assert.Implements(t, (*StorageEngine)(nil), ls)
	assert.Contains(t, ls.BasePath(), "downloads")
	assert.Equal(t, "http://127.0.0.1:3000/files/", ls.FileServerPath())
	assert.Contains(t, ls.TempDir(), "tmp")
}

func TestBuildStorageEngineS3(t *testing.T) {
	config, err := ReadConfig("./config_example_S3.yml")
	assert.Nil(t, err)
	s3, err := config.BuildStorageEngine()
	assert.Nil(t, err)
	assert.Implements(t, (*StorageEngine)(nil), s3)
	assert.Equal(t, "/episode-files/", s3.BasePath())
	assert.Equal(t, "https://files.example.com/", s3.FileServerPath())
	assert.Equal(t, "/tmp/podcastmaker", s3.TempDir())
}

func TestBuildStorageEngineNullStorage(t *testing.T) {
	config, err := ReadConfig("./config_example_NullStorage.yml")
	assert.Nil(t, err)
	se, err := config.BuildStorageEngine()
	assert.NotNil(t, err)
	err = se.CreateDirectory("")
	assert.NotNil(t, err)
}
