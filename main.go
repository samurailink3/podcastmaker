package main

import (
	"fmt"
	"os"

	pm "gitlab.com/samurailink3/podcastmaker/lib/podcastmaker"
)

func main() {
	config, err := pm.Init()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	storageEngine, err := config.BuildStorageEngine()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	pm.StartServer(*config, storageEngine)
}
