# PodcastMaker

PodcastMaker is a program designed to convert YouTube channels into podcast
feeds, complete with hosted video enclosures. It runs statelessly, meaning it
does not need a backing database and should run deterministically each time. It
offers two storage options, Local Storage or any S3-compatible storage.

## Building

Install [`ffmpeg`](https://ffmpeg.org/download.html) and
[`yt-dlp`](https://github.com/yt-dlp/yt-dlp). Make sure both are in your path.

If using a config file: create and save your config file as `config.yml`. Build
`podcastmaker` with `go build`. Either run `podcastmaker -c config.yml` or set
all the flags according to what StorageEngine you're using.

## Configuration

### Flags

You can either use flags or a config file to configure podcastmaker options. To
see a full list of flags and defaults, run: `podcastmaker -h`.

### Config File

The config file layout and options are well documented in
`lib/podcastmaker/config.go`, that is your definitive source for all options.
**If you're looking for more advanced features make sure to read this file.**

Example configurations for LocalStorage and S3 are available at
`config_example_LocalStorage.yml` and `config_example_S3.yml` respectively.

## Usage

Browse to a URL like http://127.0.0.1:3000/youtube/user/videogamedunkey (or
whatever domain you're hosting podcastmaker at). You'll see `podcastmaker`
output detailing which videos it needs to download.

Because `podcastmaker` runs statelessly, it will download each video in sequence
before generating a feed. This allows `podcastmaker` to use podcast apps
automatic refreshes as a cheap cron substitute. As these applications
automatically check in to refresh feeds, `podcastmaker` fires and builds a brand
new feed, downloading all new videos in the process. This means that the time
between a YouTube upload and `podcastmaker` knowing about it tends to be short.
**The downside of this behavior is that building a feed for the first time can
take 10-30 minutes, depending on video size and bandwidth limitations.**

## Features

### Cache Busting

If you need a new URL for the same feed, you can add random text to the end of your URL, like this:

`/youtube/user/USER_NAME/some-random-text`

This URL will give you the same content as: `/youtube/user/USER_NAME`

This is useful for cache-busting or if your feeds have broken in your podcast
app of choice. Just change the random text and you have a totally 'new' feed,
without needing to re-download videos.

## Docker

We've included some `Dockerfile`s to make things easier.

### TestAndBuild

This docker container can be used to easily Test and Build PodcastMaker.

Example build command: `docker build -t podcastmaker/pmbat:latest Dockerfiles/TestAndBuild/`  
Example run command: `docker run -it -v $(pwd):/home/builder podcastmaker/pmbat:latest`

When this finishes, you'll have a `podcastmaker` binary for Linux x64.

### Application

It will bundle `podcastmaker` and your `config.yml` into a container. Make sure
to fill in your `config.yml` file and `GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go
build` first (or you can use the TestAndBuild Dockerfile). Copy these into the
`Dockerfiles/Application` directory so they can be built into the container.

Example build command: `docker build -t podcastmaker/pmapp:latest Dockerfiles/Application/`

**If you are using LocalStorage: USE DOCKER VOLUMES**

Docker container storage is meant to be ephemeral, it **WILL GO AWAY IF A
CONTAINER RESTARTS**. This means all that time you spent downloading videos will
have been wasted. If you want to use Docker and LocalStorage, use [Docker
Volumes](https://docs.docker.com/storage/volumes/).

## Helpful Tips

PodcastMaker utilizes [S3 Multipart
Uploads](https://docs.aws.amazon.com/AmazonS3/latest/userguide/mpuoverview.html)
to support video files larger than 5 GB. Because upload failures can leave
incomplete upload parts behind, a bucket policy can help keep things clean and
your costs down.

**Bucket policy to delete incomplete multipart upload parts after one day:**

```
{
    "Rules": [
        {
            "Status": "Enabled", 
            "Prefix": "", 
            "ID": "AbortIncompleteMultipartUpload", 
            "AbortIncompleteMultipartUpload": {
                "DaysAfterInitiation": 1
            }
        }
    ]
}
```

You can utilize the [AWS CLI](https://aws.amazon.com/cli/) to add this policy to
your bucket:

* `aws s3api put-bucket-lifecycle-configuration --endpoint https://your-s3-endpoint.example.com --bucket example-bucket --lifecycle-configuration file://policy.json`

## YT-DLP Options

Here are our YT-DLP options and why we're using them:

* `--newline`: This makes Docker logs nicer to read.
* `--sleep-interval 30`: Sleep at least this many seconds before downloading a video.
* `--max-sleep-interval 60`: Because we also set `--sleep-interval`, `yt-dlp` will wait a random value between `--sleep-interval` and `--max-sleep-interval`. We need this option because YouTube will start to throttle or block `yt-dlp` if it works too fast.
* `-f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4'`: Download the best audio and video that can be converted to MP4 or the best MP4 format.
* `-o filepath`: Write the parts and combined file to this path and filename.
* `url`: The URL of the YouTube video.

## License

Inspired by the [license of `yt-dlp` itself](https://github.com/yt-dlp/yt-dlp/blob/master/LICENSE), `podcastmaker` is contributed to the Public Domain and uses the [Unlicense](https://unlicense.org/). Essentially: Fork this, steal it, copy it, remix it, sell it, build a cloud service out of it, give us credit, or not. Whatever you want to do (or not do) with this software is up to you, without restriction.
