#!/bin/sh

# This script is designed to be used as an all-in-one tester and builder. Mostly
# helpful for the BuildAndTest Dockerfile. You probably don't need to use this
# directly, since Go makes testing and building REALLY easy.

cd lib/podcastmaker/ || exit
if ! go test -count=1 -v; then
  echo "Testing library has failed!"
  exit 1
fi

if ! golangci-lint run ./... -v; then
  echo "Linting library has failed!"
  exit 1
fi

cd ../../ || exit

if ! golangci-lint run ./... -v; then
  echo "Linting main has failed!"
  exit 1
fi

if ! GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -trimpath; then
  echo "Building has failed!"
  exit 1
fi

echo "Test and Build COMPLETE!"
exit 0
