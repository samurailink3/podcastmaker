module gitlab.com/samurailink3/podcastmaker/utilities/pm-dap

go 1.16

require gitlab.com/samurailink3/podcastmaker/lib/podcastmaker v0.0.0

replace gitlab.com/samurailink3/podcastmaker/lib/podcastmaker v0.0.0 => ../../lib/podcastmaker
