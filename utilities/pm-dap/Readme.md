# pm-dap

The "PodcastMaker Download and Push" utility. Use this to re-download and
re-upload videos, just in case something got broken.

## Usage

`pm-dap` uses the same flags as `podcastmaker` and requires a single additional
argument to download-and-push a video to your `StorageEngine`: `-url`.

Use it like this: `pm-dap -c config.yml -url https://www.youtube.com/watch?v=ZZ5LpwO-An4`

**NOTE:** `pm-dap` will automatically remove backslash characters from your URL.
