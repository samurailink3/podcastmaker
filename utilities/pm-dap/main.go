package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	pm "gitlab.com/samurailink3/podcastmaker/lib/podcastmaker"
)

func main() {
	url := flag.String("url", "", "the url of the video you would like to download and push")

	config, err := pm.Init()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	storageEngine, err := config.BuildStorageEngine()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if len(*url) == 0 {
		fmt.Println("You must include a video url to download with the `-url` flag")
		flag.PrintDefaults()
		os.Exit(1)
	}

	// Remove backspaces from URL, some shells like zsh will helpfully escape
	// URLs
	*url = strings.ReplaceAll(*url, `\`, "")

	err = DownloadAndPush(*url, storageEngine)
	if err != nil {
		log.Fatal(err)
	}
}

// DownloadAndPush will download the video from the URL argument and upload it
// to your configured StorageEngine.
func DownloadAndPush(url string, storageEngine pm.StorageEngine) (err error) {
	err = pm.DownloadVideo(url, storageEngine, false)
	if err != nil {
		return err
	}
	return nil
}
